import React from 'react';
import classNames from 'classnames';
import Facebook from '../../../assets/GoldIcons/Facebook.png';
import Instagram from '../../../assets/GoldIcons/Instagram.png';
import LinkedIn from '../../../assets/GoldIcons/LinkedIn.png';

/** @jsx jsx */
import { jsx, css } from '@emotion/react'

const FooterSocial = ({
  className,
  ...props
}) => {

  const classes = classNames(
    'footer-social',
    className
  );

  const SocialStyle = css`
  height: 42px;
  width: 42px;`;

  return (
    <div
      {...props}
      className={classes}
    >
      <ul className="list-reset">
        <li>
          <a href="https://facebook.com/">
           <img src={Facebook} alt="" css={SocialStyle}/>
          </a>
        </li>
        <li>
          <a href="https://twitter.com/">
            <img src={Instagram} alt="" css={SocialStyle}/>  
          </a>
        </li>
        <li>
          <a href="https://google.com/">
           <img src={LinkedIn} alt="" css={SocialStyle}/>
          </a>
        </li>
      </ul>
    </div>
  );
}

export default FooterSocial;